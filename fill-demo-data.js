const mongoose = require("mongoose");
const { setupDBConnection } = require("./database.js");
const { Task } = require('./tasks/tasks-model.js');

const MONGODB_CONNECTION_STRING = process.env.MONGODB_CONNECTION_STRING || "mongodb://127.0.0.1/tasks-app";

fillDatabase();

async function fillDatabase() {
    await setupDBConnection(MONGODB_CONNECTION_STRING, true);
    
    console.log("Start filling database with demo data...");

    await Task.create(
        { subject: "Create demo app", tag: "GitLab", priority: "High"},
        { subject: "Prepare documentation", tag: "GitLab", deadline: "2022-11-22"},
        { subject: "Reserve table for lunch", tag: "Social", deadline: "2022-11-23"},
        { subject: "Shop coffee and snacks", deadline: "2022-11-22"});

    console.log("Finished filling database!")
    await mongoose.disconnect();
}
