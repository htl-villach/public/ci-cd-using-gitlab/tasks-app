const mongoose = require('mongoose');

const tasksSchema = new mongoose.Schema({
  subject: {
    type: String,
    required: [true, 'subject is missing'],
    trim: true,
  },
  tag: {
    type: String,
    required: false,
    index: true
  },
  deadline: {
    type: Date,
    required: false
  },
  priority: {
    type: String,
    required: false,
    enum: ['High', 'Medium', 'Low']
  },
  color: {
    type: String,
    required: false
  }
});

const Task = mongoose.model('tasks', tasksSchema);

module.exports = { Task };


