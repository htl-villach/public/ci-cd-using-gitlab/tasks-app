const express = require('express');
const r = express.Router();
const mongoose = require('mongoose');
const { Task } = require('./tasks-model');
const { logger } = require('../logging');


r.get('/', async (req, resp) => {
    try {
        logger.debug(`Tasks - Fetching tasks with filter `, req.query);

        // blindly accept filter from client - never do this in production
        let resultSet = await Task.find(req.query);
        resp.status(200);
        resp.json(resultSet);
    }
    catch (err) {
        logger.warn(`Tasks - Error getting task`, err);
        resp.status(500).send();
    }
});

r.post('/', async (req, resp) => {
    try {
        logger.debug(`Tasks - Adding new`);
        let newTask = await Task.create(req.body);
        resp.status(201);
        resp.location(req.baseUrl + '/' + newTask.id);
        resp.json(newTask);
    }
    catch (err) {
        if (err instanceof mongoose.Error.ValidationError) {
            resp.status(400).type('text/plain').send('Invalid task object');
        }
        else {
            logger.warn(`Tasks - Error adding new tour`, err);
            resp.status(500).send();
        }
    }
})

r.get('/:id', async (req, resp) => {
    let taskId = req.params.id;
    try {
        logger.debug(`Task - Fetch task with id=${taskId}`);
        let task = await Task.findById(taskId);
        if (task) {
            resp.status(200).json(task);
        }
        else {
            resp.status(404).type('text/plain').send(`Task with id ${taskId} not found`);
        }
    }
    catch (err) {
        logger.warn(`Tasks - Error getting task with id=${taskId}`, err);
        resp.status(500).send();
    }
});

r.delete('/:id', async (req, resp) => {
    let taskId = req.params.id;
    try {
        logger.debug(`Tours - Delete tour with id=${taskId}`);

        let opResult = await Task.deleteOne({ _id: taskId });
        if (opResult.deletedCount == 1) {
            resp.status(204).send();
        }
        else {
            resp.status(404).type('text/plain').send(`Task with id ${taskId} not found`);
        }
    }
    catch (err) {
        logger.warn(`Tours - Error deleting task with id=${taskId}`, err);
        resp.status(500).send();
    }
});

module.exports = r;
