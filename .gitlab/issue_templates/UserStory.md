## Description

As a _..role.._ I want _..feature.._ so that _..business value.._

...further description


## Tasks

- [ ] Task 1
- [ ] Task 2
- [ ] Task n


## Acceptance Criteria

- [ ] AC 1
- [ ] AC 2


## Definition of Done

- [ ] Automated tests in place
- [ ] API documented
- [ ] User manual updated

/label ~UserStory
