const tp = require('timers/promises');
let request = require('supertest');
let server = require('../server');


async function ensureServiceIsReady(retries = 15) {

    let readyzResp = await request(server).get('/api/readyz');
    
    while (readyzResp.status != 200) {
        retries--;
        if (retries == false)
            throw new Error('retries exceeded - backend not ready');

        await tp.setTimeout(1000);
        readyzResp = await request(server).get('/api/readyz');
    }
}

module.exports = { ensureServiceIsReady }
