const request = require('supertest');
const server = require('../../server');
const { addTask, validTask } = require('./common');
const { ensureServiceIsReady } = require('../common');

describe('Tasks - DELETE', () => {

    before((done) => {
        ensureServiceIsReady()
            .then(() => done())
            .catch((err) => done(err));
    });

    beforeEach(async () => {
        await server.dropCurrentDatabase();
    })

    describe('/DELETE task by id', () => {

        it('it should return 404 for an unknown task', async () => {
            await request(server)
                .delete('/api/tasks/65326f5fec6cd87154f9db87')
                .expect(404);
        });

        it('it should delete a previously inserted task', async () => {
            const taskId = await addTask(request(server), validTask);

            await request(server)
                .delete('/api/tasks/' + taskId)
                .expect(204);            
        });

    });
});
