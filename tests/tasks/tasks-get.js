const request = require('supertest');
const assert = require('assert');
const server = require('../../server');
const { addTask, validTask, anotherValidTask } = require('./common');
const { ensureServiceIsReady } = require('../common');

describe('Tasks - GET', () => {

    before((done) => {
        ensureServiceIsReady()
            .then(() => done())
            .catch((err) => done(err));
    });


    beforeEach(async () => {
        await server.dropCurrentDatabase();
    })

    describe('/GET tasks', () => {

        it('it should initially return an empty list', async () => {
            const response = await request(server)
                .get('/api/tasks')
                .expect(200)
                .expect('Content-Type', /json/);
            assert.equal(response.body.length, 0);
        });

        it('it should return a list with a single task', async () => {

            const taskId = await addTask(request(server), validTask);

            const getResponse = await request(server)
                .get('/api/tasks')
                .expect(200)
                .expect('Content-Type', /json/);

            assert.equal(getResponse.body.length, 1);
            assert.equal(getResponse.body[0]._id, taskId);
        });

        it('it should list two tasks', async () => {

            await addTask(request(server), validTask);
            await addTask(request(server), anotherValidTask);

            const getResponse = await request(server)
                .get('/api/tasks')
                .expect(200)
                .expect('Content-Type', /json/);

            assert.equal(getResponse.body.length, 2);
        });
    });

    describe('/GET task by id', () => {

        it('it should return 404 for an unknown task', async () => {
            await request(server)
                .get('/api/tasks/65326f5fec6cd87154f9db87')
                .expect(404);
        });

        it('it should return a previously inserted task', async () => {
            const taskId = await addTask(request(server), validTask);

            const getResponse = await request(server)
                .get('/api/tasks/' + taskId)
                .expect(200)
                .expect('Content-Type', /json/);

            assert.equal(getResponse.body._id, taskId);
        });

    });
});
