const request = require('supertest');
const assert = require('assert');
const server = require('../../server');
const { validTask, getTask } = require('./common');
const { ensureServiceIsReady } = require('../common');

describe('Tasks - POST', () => {

    before((done) => {
        ensureServiceIsReady()
            .then(() => done())
            .catch((err) => done(err));
    });

    beforeEach(async () => {
        await server.dropCurrentDatabase();
    })

    describe('/POST tasks - happy flow', () => {

        it('it should add a single task', async () => {
            const postResponse = await request(server)
                .post('/api/tasks')
                .send(validTask)
                .expect(201)
                .expect('Content-Type', /json/);

            const taskId = postResponse.body._id;

            assert.equal(typeof (postResponse.body), 'object');
            assert.notEqual(taskId, undefined);
            assert.equal(postResponse.headers.location, `/api/tasks/${taskId}`);

            const task = await getTask(request(server), taskId);
            
            assert.equal(task._id, taskId);
        });

        it('it should not add a task with invalid priority', async () => {
            let invalidTask = { ...validTask, priority: 'Medium-Rare' };

            await request(server)
                .post('/api/tasks')
                .send(invalidTask)
                .expect(400);
/*
            const getResponse = await request(server)
                .get('/api/tasks')
                .expect(200)
                .expect('Content-Type', /json/);

            assert.equal(getResponse.body.length, 0);*/
        });
    });
});
