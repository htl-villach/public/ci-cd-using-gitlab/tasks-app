const assert = require('assert');

const validTask = { subject: "Create demo app", tag: "GitLab", priority: "High"};

const anotherValidTask = { subject: "Prepare documentation", tag: "GitLab", deadline: "2022-11-22"};

async function addTask(r, task) {
    const response = await r
        .post('/api/tasks')
        .send(task)
        .expect(201)
        .expect('Content-Type', /json/);

    const taskId = response.body._id;

    assert.equal(typeof (response.body), 'object');
    assert.notEqual(taskId, undefined);
    assert.equal(response.headers.location, `/api/tasks/${taskId}`);

    return taskId;
}

async function getTask(r, taskId) {
    const response = await r
        .get('/api/tasks/' + taskId)
        .expect(200)
        .expect('Content-Type', /json/);

    assert.equal(typeof (response.body), 'object');
    assert.equal(response.body._id, taskId);  

    return response.body;
}


module.exports = { validTask, anotherValidTask, addTask, getTask };
