
$(() => {
  /* Attach Eventhander */
  $('#btnAddTask').on('click', addTask);
  $('#preset').on('change', reloadTasks);
  $('#btnReloadTasks').on('click', reloadTasks);
  reloadTasks();
});

function addTask() {
  let newTask = readValuesFromUI();
  
  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status >= 200 && this.status < 399) {
        let persistedTask = JSON.parse(this.responseText); 
        reloadTasks();       
      } else {
        console.log('server responded with http error:', this);
      }
    }
  };

  // just a hardcoded-value with a query-String to test the request-handler
  xhr.open('POST', `/api/tasks`, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(newTask));
}

function undefinedIfEmpty(v) {
  return v==='' ? undefined : v;
}

function readValuesFromUI() {
  return {
    subject: $('#subject').val(),
    tag: undefinedIfEmpty($('#tag').val()),
    deadline: undefinedIfEmpty($('#deadline').val()),
    priority: undefinedIfEmpty($('#priority option:selected').val()),    
    color: undefinedIfEmpty($('#color option:selected').val())
  };
}


function reloadTasks() {

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status >= 200 && this.status < 399) {
        let allTasks = JSON.parse(this.responseText);
        renderTasks(allTasks);        
      } else {
        console.log('server responded with http error:', this);
      }
    }
  };
  
  let filterString = $('#preset option:selected').val();
  xhr.open('GET', `/api/tasks${ filterString ? "?" + filterString : ""}`, true);  
  xhr.send();
}


function renderTasks(tasks) {

  let taskList = $("#taskList")[0];

  let numTasks = $("#numTasks")[0];
  numTasks.textContent = tasks.length;

  $(taskList).empty();
  tasks.forEach((t) => renderTaskItem(taskList, t));
}

function renderTaskItem(taskList, task) {
  let taskItem = document.createElement('li');
  taskItem.setAttribute('class', 'list-group-item d-flex justify-content-between lh-sm');
  if (task.color)
    taskItem.setAttribute('style', `background-color: ${task.color};`);

  let div = document.createElement('div');
  let header = document.createElement('h6');
  header.setAttribute('class', 'my-0');
  header.textContent = task.subject;

  let tags = document.createElement('small');
  tags.setAttribute('class', 'text-muted');
  tags.textContent = task.tag || "-";

  let deadline = document.createElement('span');
  deadline.setAttribute('class', 'text-muted');
  deadline.textContent = task.deadline ? new Date(task.deadline).toLocaleDateString() : "-";

  div.appendChild(header);
  div.appendChild(tags);
  taskItem.appendChild(div);
  taskItem.appendChild(deadline);

  taskList.appendChild(taskItem);
/*
  <li class="list-group-item d-flex justify-content-between lh-sm">
  <div>
    <h6 class="my-0">Install new something</h6>
    <small class="text-muted">GitLab</small>
  </div>
  <span class="text-muted">03.12.2022</span>
</li>
*/
}


