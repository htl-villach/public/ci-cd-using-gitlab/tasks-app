'use strict';

const express = require('express');
const http = require('http');

const tasksRouter = require('./tasks/tasks-router');
const { setupHealthChecks } = require('./health-checks.js');
const { setupDBConnection, dropCurrentDatabase } = require('./database.js');
const { logger } = require('./logging');

logger.info('Backend - Starting up...');

// Take configuration from environment variables
// or use hardcoded default value
const HOST = '0.0.0.0';
const PORT = process.env.PORT || 8080;
const MONGODB_CONNECTION_STRING = process.env.MONGODB_CONNECTION_STRING || 'mongodb://127.0.0.1/tasks-app';
const MONGODB_RECREATE = process.env.MONGODB_RECREATE === 'true';
const MONGODB_FAILSTARTUP_WITHOUT_CONN = process.env.MONGODB_FAILSTARTUP_WITHOUT_CONN != undefined ? process.env.MONGODB_FAILSTARTUP_WITHOUT_CONN == 'true' : true;


const app = express();
app.use(express.json());
app.use('/api/tasks', tasksRouter);
app.use(express.static('./_client'));

// create HTTP server
const httpServer = http.createServer(app);

// establish DB connection
setupDBConnection(MONGODB_CONNECTION_STRING, MONGODB_RECREATE, MONGODB_FAILSTARTUP_WITHOUT_CONN);

// add function so that it is accessible by tests
httpServer.dropCurrentDatabase = async () => {
    await dropCurrentDatabase(MONGODB_CONNECTION_STRING);
}

// setup health check endpoints on server
setupHealthChecks(httpServer);

// start listening to HTTP requests
httpServer.listen(PORT, HOST, () => {
    logger.info(`Backend - Running on port ${PORT}...`);
});



module.exports = httpServer;
