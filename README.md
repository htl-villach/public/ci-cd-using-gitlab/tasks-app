# Tasks App

## Technical Overview

```plantuml

node "Browser" {
  [WebApp] 
}

node "Node.Js" {
  interface HTTP
  [Express]
}

database "MongoDB" {
    [DB]
}

[WebApp] -right-> HTTP
HTTP -right- [Express]
[Express] -right- DB
```

This application is based on a simplification of the MEAN or MERN stack
consisting of the following technologies:

* MongoDB
* Express
* a simple website instead of Angular or React
* Node.js

The frontend is based on a set of HTML, CSS and JS files. It accesses
the backend via API REST endpoints. The API is handled by the Express
framework running on top of Node.js. Also the frontend itself is served
by Express which makes containerization, deployments for educational purposes
by far less complex.

The app is far from being perfect and intentionally lacks a lot of
aspects like logging, authorization and authentication, centralized
error handling, input validation and many more.

## Getting started

### How to clone

```shell
# clone this repo to your file system
git clone https://www.gitlab.com/.../tasks-app.git

# switch into the repo's folder
cd tasks-app

# install all the dependencies as specified in the package.json
npm install
```

### Managing the MongoDB container

In order to test or run the application, a MongoDB instance is required.
For development purposes the easiest approach is to use a MongoDB container image.
The default configuration assumes that a instance is running on `127.0.0.1` on the
default port `27017`.

Running and testing the application will use different
connection strings pointing to different databases so that testing will not interfere.

* For testing the app `mongodb://127.0.0.1/tasks-app-test`
* For running the app `mongodb://127.0.0.1/tasks-app`

The configuration can be overwritten using the environment variable `MONGODB_CONNECTION_STRING`.

```shell
# initially, a container can be created as follows
# this needs to be done only once
docker create --name tasks-db -p 27017:27017 mongo:7
7311dee494ae8de0c8b5d9c0969b42b076c6d2ddea6c590e3cc2fa5b7ab5400e

# the container can be started as follows and will run in background
docker start tasks-db

# running containers can be listed by using
docker ps

CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS         PORTS                      NAMES
7311dee494ae   mongo:7   "docker-entrypoint.s…"   14 seconds ago   Up 4 seconds   0.0.0.0:27017->27017/tcp   tasks-db

# the container can be stopped using
docker stop tasks-db

# and ultimatively removed using
docker rm -f tasks-db
```

The lifecycle of the container is as follows

```plantuml

state Container 

state "Container tasks-db" as Container {
  state Created
  state Up
  state Exited
  Created -right-> Up: docker start
  Up -right-> Exited: docker stop
  Exited -left-> Up: docker start
}

[*] -down-> Created: docker create
Container -down-> [*]: docker rm -f

```

### How to test

```shell
# ensure that the MongoDB instance is running

# run tests as often as you want
# will drop database every time
npm test
```

### How to run

```shell
# ensure that the MongoDB instance is running

# fill database with demo data
# this command will wipe out the current content of the database
npm run fill-demo-data

# start the service
npm start

# and open a browser pointing to http://127.0.0.1:8080
```
